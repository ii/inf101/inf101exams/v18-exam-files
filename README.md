# Filer til eksamen V18

Her finner du alle .java filene som er gitt med eksamen H21.

Merk at denne eksamenen ikke hadde kompilator tilgjengelig. Noen vedlagte filer er likevel implementert som klasser i dette prosjektet for å gjøre det mulig å gjøre alle oppgavene i editor. 

Nevnte klasser som ikke er implementert er enten vedlagt på bunnen av eksamensbeskrivelsen eller er klasser du selv må opprette. Klassene MyBox og IPosition må du selv implementere, men filene er opprettet allerede for å unngå kompileringsfeil i andre klasser.