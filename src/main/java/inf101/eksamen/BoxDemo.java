package inf101.eksamen;

public class BoxDemo {
    public static void main(String[] args) {
        int a;
        IBox<Integer> b = new MyBox<>();
        IBox<Integer> c = new MyBox<>();
        IBox<IBox<Integer>> d = new MyBox<>();

        a = 5;
        b.put(5);
        c.put(5);
        d.put(c);

        m1(a);
        m2(b);
        m2(c);
        m3(d);

        System.out.println("a=" + a);
        System.out.println("b.get()=" + b.get());
        System.out.println("c.get()=" + c.get());
        System.out.println("d.get().get()=" + d.get().get());
    }
    
    static void m1(int x) {
        x = x+1;
    }

    static void m2(IBox<Integer> box) {
        box.put(box.get()+1);
    }

    static void m3(IBox<IBox<Integer>> box) {
        m2(box.get());
    }
}
