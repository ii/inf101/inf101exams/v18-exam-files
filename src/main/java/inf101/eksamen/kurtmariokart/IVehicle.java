package inf101.eksamen.kurtmariokart;

public interface IVehicle {
    
    /**
     * @return The current position
     */
    IPosition getPosition();

    /**
     * @return The current speed (in z-units per step())
     */
    double getSpeed();

    /**
     * @return An identifying symbol
     */
    default String getSymbol() {
        return  "🦆";
    }

    /**
     * Perform one time step.
     * 
     * In a normal time step, the vehicle's z-position will increase according to
     * the current speed. (Other things might also happen)
     */
    void step();

    /**
     * Change the x-position by the given amount.
     * 
     * @param amount X adjustment (positive=right, negative=left)
     */
    void steer(double amount);

    /**
     * Increase the speed by the given amount.
     * @param amount Speed increase; must be >= 0
     */
    void accelerate(double amount);

    /**
     * Decrease the speed by the given amount.
     * @param amount Speed decrease; must be >= 0
     */
    void bake(double amount);
}
