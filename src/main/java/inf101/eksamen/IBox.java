package inf101.eksamen;

/**
 * An interface for a box data structure. A box can store a 
 * single data element for later retrieval.
 * 
 * @param <T> Type of data
 */
public interface IBox<T> {
    /**
     * Store an element in the box.
     * 
     * Will replace the previous element, if any. Afterwards,
     * <code>get() == elem</code>.
     * 
     * Behaviour is undefined if <code> elem == null</code>
     * 
     * @param elem An element
     */
    void put(T elem);

    /**
     * Retrieve element from the box.
     * 
     * Will return the argument of the last call to {@link #put(T)}.
     * 
     * Behaviour is undefined if {@link #isEmpty()} return true.
     * 
     * @return The previously stored element.
     * @requires !isEmpty()
     */
    T get();

    /**
     * @return True if the box contains an element
     */
    boolean isEmpty();
}