package inf101.eksamen;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class BoxTest {
    
    @Test
    void test() {
        IBox<String> a = new MyBox<>();
        IBox<String> b = new MyBox<>();
        a.put("eple");
        b.put("eple");

        assertEquals(a, b);
    }
}
